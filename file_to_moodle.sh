HASH='b476facbf5427ea4cc00df2bd4428a56114f34b0'
FILE='/home/lucas/Downloads/ingresso_bahia_gremio_cb_lucas.pdf'
ROOT_PATH='/var/lib/docker/volumes/moodle-docker_moodle_data/_data/moodledata/filedir'
if [[ $HASH == $(sha1sum $FILE | cut -f1 -d ' ') ]]
then
    NEW_DIR="${ROOT_PATH}/${HASH:0:2}/${HASH:2:2}"
    mkdir -pv $NEW_DIR
    cp -v "$FILE" "${NEW_DIR}/$HASH"
    chown -R bin:bin $ROOT_PATH
    chmod -R 664 $ROOT_PATH
fi


